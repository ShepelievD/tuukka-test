const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const PASSWORD_REGEXP = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/

export const mustBeEmail = value => {
  if (value && !EMAIL_REGEXP.test(value)) {
    return 'Should be email'
  }
}

export const mustBeRequired = value => {
  if (!value) {
    return 'Required'
  }
}

export const mustBePassword = value => {
  if (value && !PASSWORD_REGEXP.test(value)) {
    return 'Must contain at least 1 lowercase, uppercase ... character'
  }
}
