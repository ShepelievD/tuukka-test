Prerequisites:
1. [Node version manager](https://github.com/creationix/nvm)

1. Run `nvm use`
1. Run `npm i`
1. Run `npm run dev`
1. Visit [http://localhost:3000](https://github.com/creationix/nvm)
