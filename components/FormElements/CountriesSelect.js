import React, { useState } from 'react'
// libs
import client from 'axios'
import orderBy from 'lodash/orderBy'
// constants
import { COUNTRIES_ENPOINT } from '../../constants'

const CountriesSelect = (props) => {
  const [entities, setEntities] = useState([])

  useState(async () => {
    try {
      const { data } = await client.get(COUNTRIES_ENPOINT)

      setEntities(orderBy(data, 'country'))
    } catch (errors) {
      console.log('[CountriesSelect.fetchCountries]: ', errors)
    }
  }, [])

  return (
    <select {...props}>
      {entities.map(({ abbreviation, country }) => <option key={abbreviation} value={abbreviation}>{country}</option>)}
    </select>
  );
};

export default CountriesSelect;
