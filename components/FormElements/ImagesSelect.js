import React, { useState } from 'react'
// libs
import client from 'axios'
// constants
import { IMAGES_ENPOINT } from '../../constants'

const ImagesSelect = (props) => {
  const [entities, setEntities] = useState([])

  useState(async () => {
    try {
      const { data } = await client.get(IMAGES_ENPOINT)

      setEntities(data)
    } catch (errors) {
      console.log('[ImagesSelect.fetchImages]: ', errors)
    }
  }, [])

  return (
    <select {...props}>
      {entities.map((value) => <option value={value} key={value}>{value}</option>)}
    </select>
  );
};

export default ImagesSelect;
