import React, { useState } from 'react';
// components
import { SingleDatePicker } from 'react-dates'

const DatePicker = ({ name, value, onChange, ...rest }) => {
  const [isFocused, setFocused] = useState(false)

  return (
    <SingleDatePicker
      id={name}
      onDateChange={onChange}
      date={value}
      focused={isFocused}
      onFocusChange={({ focused }) => setFocused(focused)}
      {...rest}
    />
  );
};

export default DatePicker;
