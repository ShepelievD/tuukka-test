import React from 'react'
// components
import { Clocker, UserForm } from '../components'

import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'

const IndexPage = () => {
  return (
    <>
      <div>
        <Clocker />
        <UserForm />
      </div>
    </>
  )
}

export default IndexPage
