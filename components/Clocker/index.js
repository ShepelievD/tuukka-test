import React, { Component } from 'react'

class Clocker extends Component {
  intervalId = null

  state = {
    time: 0
  }

  componentDidMount () {
    this.intervalId = setInterval(() => {
      this.setState({ time: this.state.time + 1000 })
    }, 1000)
  }

  componentWillUnmount() {
    clearInterval(this.intervalId)
  }

  render () {
    const { time } = this.state

    return time
  }
}

export default Clocker

