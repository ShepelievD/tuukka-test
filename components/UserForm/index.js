import React, { useState } from 'react'
// libs
import moment from 'moment'
// components
import { Input, DatePicker, CountriesSelect, ImagesSelect } from '../FormElements'
// utils
import { mustBeRequired, mustBeEmail, mustBePassword } from '../../utils/validations'

const UserForm = () => {
  const [values, setValues] = useState({
    email: '',
    password: '',
    date: moment(),
    country: '',
    picture: ''
  })

  const [errors, setErrors] = useState({
    email: [],
    password: [],
    date: [],
    country: [],
    picture: []
  })

  const setValue = (name, value, validations = []) => {
    const fieldErrors = []

    validations.forEach(rule => {
      const error = rule(value)
      if (error) {
        fieldErrors.push(error)
      }
    })

    setValues({
      ...values,
      [name]: value
    })
    setErrors({
      ...errors,
      [name]: fieldErrors
    })
  }

  const handleFieldChange = (name, validations = []) => ({ target: { value } }) => setValue(name, value, validations)
  const handleDateChange = value => setValue('date', value, [])

  const handleFormSubmit = (event) => {
    event.preventDefault()

    console.log('form is submitted: ', values)
  }

  const isFormDisabled = Object.values(errors).some(array => !!array.length)

  return (
    <form onSubmit={handleFormSubmit}>
      <div>
        <Input name={'email'} type={'email'} value={values.email} onChange={handleFieldChange('email', [mustBeRequired, mustBeEmail])} />
        {errors.email.join('. ')}
      </div>
      <div>
        <Input name={'password'} type={'password'} value={values.password} onChange={handleFieldChange('password', [mustBeRequired, mustBePassword])} />
        {errors.password.join('. ')}
      </div>
      <div>
        <DatePicker name={'date'} value={values.date} onChange={handleDateChange} />
        {errors.date.join('. ')}
      </div>
      <div>
        <CountriesSelect name={'counties'} value={values.country} onChange={handleFieldChange('country', [mustBeRequired])} />
        {errors.country.join('. ')}
      </div>
      <div>
        <ImagesSelect name={'picture'} value={values.picture} onChange={handleFieldChange('picture', [mustBeRequired])} />
        {errors.picture.join('. ')}
      </div>
      <button disabled={isFormDisabled}>Submit!</button>
    </form>
  )
}

export default UserForm
